python-sunlight (1.1.5-5) UNRELEASED; urgency=medium

  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 11 Jan 2023 12:28:40 -0000

python-sunlight (1.1.5-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Apply multi-arch hints.
    + python-sunlight-doc: Add Multi-Arch: foreign.
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Submit.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 12:24:13 -0400

python-sunlight (1.1.5-3) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.
  * Use Python 3 for building docs.
  * Drop Python 2 support.
  * Use pybuild for building package.
  * Enable autopkgtest-pkg-python testsuite.
  * Bump debhelper compat level to 12.
  * d/watch: Use pypi.debian.net.

 -- Ondřej Nový <onovy@debian.org>  Fri, 09 Aug 2019 11:52:00 +0200

python-sunlight (1.1.5-2) unstable; urgency=high

  * Team upload.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Piotr Ożarowski ]
  * Add dh-python to Build-Depends

  [ Carsten Schoenert ]
  * Fix build of documentation. (Closes: #922261)

  [ Dominik George ]
  * Bump Standards-Version (no changes needed).
  * Raise compat level to 11.

 -- Dominik George <natureshadow@debian.org>  Sat, 23 Feb 2019 20:51:16 +0100

python-sunlight (1.1.5-1) unstable; urgency=low

  * New upstream release. Record time. Handles a fix with passing boolean
    values into the kwargs.

 -- Paul Tagliamonte <paultag@debian.org>  Fri, 29 Jun 2012 16:58:53 -0400

python-sunlight (1.1.4-1) unstable; urgency=low

  * New upstream release. Adds some better UTF handling in OpenStates.

 -- Paul Tagliamonte <paultag@debian.org>  Fri, 29 Jun 2012 16:22:21 -0400

python-sunlight (1.1.3-1) unstable; urgency=low

  * New upstream release. Adds a new method to the OpenStates object, and
    adds a more helpful exception when you attempt to pass "None" into an
    OpenStates method.

 -- Paul Tagliamonte <paultag@ubuntu.com>  Tue, 15 May 2012 23:04:51 -0400

python-sunlight (1.1.2-1) unstable; urgency=low

  * New upstream release. Solves a URL encoding issue that was present in both
    CapitolWords and Influence Explorer.

 -- Paul Tagliamonte <paultag@ubuntu.com>  Thu, 12 Apr 2012 16:57:24 -0400

python-sunlight (1.1.1-1) unstable; urgency=low

  * New upstream release. Contains fixes for Python 3, and fixed examples.
  * Changing an errorful link (python-sunlight3 -> python3-sunlight)
  * Ensuring that SOURCES.txt is removed from the binary packages (thanks,
    jwilk & tumbleweed)

 -- Paul Tagliamonte <paultag@ubuntu.com>  Mon, 27 Feb 2012 15:22:20 -0500

python-sunlight (1.1.0-1) unstable; urgency=low

  * Initial release (Closes: #660173).

 -- Paul Tagliamonte <paultag@ubuntu.com>  Mon, 27 Feb 2012 20:10:53 +0200
